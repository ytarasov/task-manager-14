package ru.t1.ytarasov.tm.service;

import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String taskId, final String projectId) {
        if (taskId == null || taskId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String id) {
        if (id == null || id.isEmpty()) return;
        final Project project = projectRepository.findOneById(id);
        if (project == null) return;
        List<Task> tasksToRemove = taskRepository.findAllTasksByProjectId(project.getId());
        for (Task task : tasksToRemove) taskRepository.removeById(task.getId());
        projectRepository.removeById(id);
    }

    @Override
    public void unbindTaskFromProject(final String taskId, final String projectId) {
        if (taskId == null || taskId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        final Task task = taskRepository.findOneById(taskId);
        if (!projectRepository.existsById(projectId)) return;
        if (task == null) return;
        task.setProjectId(null);
    }

}
