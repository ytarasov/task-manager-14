package ru.t1.ytarasov.tm.controller;

import ru.t1.ytarasov.tm.api.controller.IProjectTaskController;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(taskId, projectId);
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        projectTaskService.unbindTaskFromProject(taskId, projectId);
        System.out.println("[OK]");
    }

}
