package ru.t1.ytarasov.tm.api.service;

import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, Status status);

    Task create(String name, String description, Status status);

    void clear();

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    List<Task> findAllTasksByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeProjectStatusById(String id, Status status);

    Task changeProjectStatusByIndex(Integer index, Status status);

    Task removeById(String id);

    Task removeByIndex(Integer index);
}
