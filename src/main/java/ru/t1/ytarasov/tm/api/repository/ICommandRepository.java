package ru.t1.ytarasov.tm.api.repository;

import ru.t1.ytarasov.tm.model.Command;

public interface ICommandRepository {
    Command[] getTerminalCommands();
}
